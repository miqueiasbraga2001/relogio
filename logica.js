const horas = $('.horas'); 
const minutos = $('.minutos'); 
const segundos = $('.segundos'); 


let setRotacao = (e, rotacaoP) => {
    //e.style.setProperty("--rotacao", rotacaoP * 360);
    e.css('--rotacao',rotacaoP * 360)
}

function hora_atual(){
    let hotario = new Date();

    let segundosP = hotario.getSeconds() / 60;
    let minutosP = (segundosP + hotario.getMinutes()) / 60;
    let horasP = (minutosP + hotario.getHours()) / 12;

    setRotacao(horas,horasP);
    setRotacao(minutos,minutosP);
    setRotacao(segundos,segundosP);
}
setInterval(hora_atual, 1000);
